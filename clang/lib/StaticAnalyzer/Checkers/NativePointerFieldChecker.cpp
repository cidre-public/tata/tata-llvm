#include "clang/StaticAnalyzer/Checkers/BuiltinCheckerRegistration.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugType.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CallEvent.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"
#include "llvm/ADT/StringRef.h"
#include "Taint.h"
#include <utility>
#include <string.h>
#include <iostream>
#include <regex>

using namespace clang;
using namespace ento;
using namespace taint;

namespace {
  enum OType {CID=1, FID, FIELD, ARG};
  class ObjType {
  public:
    virtual OType getType() const = 0;
    virtual bool operator==(const ObjType &X) const = 0;
    virtual void Profile(llvm::FoldingSetNodeID &ID) const = 0;
    bool operator<(const ObjType &other) const {
      llvm::FoldingSetNodeID ID;
      llvm::FoldingSetNodeID IDo;
      Profile(ID);
      other.Profile(IDo);
      return ID < IDo;
    }  
  };

  class Arg : public ObjType {
  public:
    std::string funcname;
    unsigned int position;
    Arg(std::string fn, unsigned int pos) : funcname(fn), position(pos) {}
    OType getType() const {return ARG;}
    bool operator==(const Arg &X) const {
      return funcname == X.funcname && position  == X.position;
    }
    bool operator==(const ObjType &X) const {
      return false;
    }
    void Profile(llvm::FoldingSetNodeID &ID) const {
      ID.AddString(funcname);
      ID.AddInteger(position);
    }
  };

  class ClassID : public ObjType {
  public:
    std::string class_identifier;
    ClassID(std::string cls_id) : class_identifier(cls_id) {}
    OType getType() const {return CID;}
    bool operator==(const ClassID &X) const {
      return class_identifier == X.class_identifier;
    }
    bool operator==(const ObjType &X) const {
      return false;
    }
    void Profile(llvm::FoldingSetNodeID &ID) const {
      ID.AddString(class_identifier);
    }
  };

  class FieldID : public ObjType {
  public:
    ClassID* cid;
    std::string field_identifier;
    FieldID(ClassID* cid, std::string fld_id) : cid(cid), field_identifier(fld_id) {}
    OType getType() const {return FID;}
    bool operator==(const FieldID &X) const {
      return cid == X.cid && field_identifier == X.field_identifier;
    }
    bool operator==(const ObjType &X) const {
      return false;
    }
    void Profile(llvm::FoldingSetNodeID &ID) const {
      llvm::FoldingSetNodeID IDcid;
      cid->Profile(IDcid);
      ID.AddNodeID(IDcid);
      ID.AddString(field_identifier);
    }
  };

  class Field : public ObjType {
  public:
    FieldID* fid;
    Field(FieldID* fid) : fid(fid) {}
    OType getType() const {return FIELD;}
    FieldID* getFieldID() const {return fid;}
    bool operator==(const Field &X) const {
      return fid == X.fid;
    }
    bool operator==(const ObjType &X) const {
      return false;
    }
    void Profile(llvm::FoldingSetNodeID &ID) const {
      llvm::FoldingSetNodeID IDfid;
      fid->Profile(IDfid);
      ID.AddNodeID(IDfid);
    }
  };

  class NativePointerFieldChecker : public Checker<check::PostCall,
						   check::PreCall,
						   check::PreStmt<CastExpr>,
						   check::PreStmt<ReturnStmt>,
						   check::PreStmt<BinaryOperator>,
						   check::PostStmt<ImplicitCastExpr>,
						   check::PostStmt<BinaryOperator>,
						   check::PostStmt<Expr>> {
    
    mutable std::set<Arg> pointerArgs;
    mutable std::set<std::string> pointerReturnValues;
    mutable std::set<FieldID> pointerFields;
    
  public:
    NativePointerFieldChecker() {};
    ~NativePointerFieldChecker();

    void checkPostCall(const CallEvent &Call, CheckerContext &C) const;
    void checkPreCall(const CallEvent &Call, CheckerContext &C) const;
    void checkPreStmt(const CastExpr *CE, CheckerContext &C) const;
    void checkPreStmt(const ReturnStmt *S, CheckerContext &C) const;
    void checkPreStmt(const BinaryOperator *S, CheckerContext &C) const;
    void checkPostStmt(const ImplicitCastExpr *ICE, CheckerContext &C) const;
    void checkPostStmt(const BinaryOperator *S, CheckerContext &C) const;
    void checkPostStmt(const Expr *E, CheckerContext &C) const;
  };

  class DefinedSValWrapper {
    const DefinedSVal sval;
  public:
    DefinedSValWrapper(const DefinedSVal& sv): sval(sv) {}
    const DefinedSVal& get() const { return sval; }
    void Profile(llvm::FoldingSetNodeID& ID) const { sval.Profile(ID); }
    bool operator ==(const DefinedSValWrapper& RHS) const { return sval == RHS.sval; }
    bool operator <(const DefinedSValWrapper& RHS) const {
      llvm::FoldingSetNodeID idLHS, idRHS;
      Profile(idLHS);
      RHS.Profile(idRHS);
      return idLHS < idRHS;
    }
  };
}

REGISTER_MAP_WITH_PROGRAMSTATE(SValObjTypeMap, DefinedSValWrapper, ObjType*)
REGISTER_SET_WITH_PROGRAMSTATE(IsPreTainted, const BinaryOperator*)
REGISTER_SET_WITH_PROGRAMSTATE(TaintedExpr, const Expr*)
REGISTER_SET_WITH_PROGRAMSTATE(TaintedSVal, DefinedSValWrapper)

REGISTER_TRAIT_WITH_PROGRAMSTATE(WatchCounter, unsigned)
REGISTER_MAP_WITH_PROGRAMSTATE(WatchMap, unsigned long, ObjType*)

bool isSValTainted(const SVal sv, ProgramStateRef state) {
  if(Optional<DefinedSVal> defSVal = sv.getAs<DefinedSVal>())
    if(state->contains<TaintedSVal>(*defSVal))
      return true;
  if(Optional<Loc> stSymAsLoc = sv.getAs<Loc>()) {
    StoreManager& sm = state->getStateManager().getStoreManager();
    SVal storedSVal = sm.getBinding(state->getStore(), *stSymAsLoc);
    if(Optional<DefinedSVal> defSVal = storedSVal.getAs<DefinedSVal>())
      if(state->contains<TaintedSVal>(*defSVal))
	return true;
  }
  return false;
}

bool containsPointer(const Stmt* st, CheckerContext &C) {
  ProgramStateRef state = C.getState();
  
  const Expr* e = dyn_cast<Expr>(st);
  if(e && (e->getType().getTypePtr()->isPointerType() || state->contains<TaintedExpr>(e)))
    return true;

  if(isSValTainted(C.getSVal(st), state))
    return true;

  if(dyn_cast<Expr>(st) && !dyn_cast<CallExpr>(st))
    for (auto st_it = st->child_begin(); st_it != st->child_end(); ++st_it)
      if(containsPointer(*st_it, C))
	return true;
  
  return false;
}

std::string getFunctionNameForDecl(const Decl* d) {
  if (const FunctionDecl* fd = dyn_cast<FunctionDecl>(d)) {
    return fd->getNameInfo().getName().getAsString();
  } else {
    llvm::errs() << "ERROR: unhandled case.\n";
    llvm::errs() << "\t";d->dump();llvm::errs() << "\n";
    return "ERROR unhandled decl type.";
  }
}

void NativePointerFieldChecker::checkPostCall(const CallEvent &Call,
					      CheckerContext &C) const {
  ProgramStateRef state = C.getState();
  Optional<DefinedSVal> ret = Call.getReturnValue().getAs<DefinedSVal>();
  if(!ret) return;
  if(Call.getDecl()) {
    if(!strcmp(Call.getDecl()->getDeclKindName(), "CXXMethod")) {
      const FunctionDecl* fd = (const FunctionDecl*) Call.getDecl();
      std::string funcname = fd->getNameInfo().getAsString();
      ProgramStateRef state = C.getState();

      if(!funcname.compare("FindClass")) {
	const clang::ento::MemRegion* memreg = Call.getArgSVal(0).getAsRegion();
	if(!memreg) return;

	const clang::ento::ElementRegion* elmtreg = memreg->getAs<clang::ento::ElementRegion>();
	if(!elmtreg) return;

	std::string cls_id = elmtreg->getAsArrayOffset().getRegion()->getString();

	ClassID* cid_ptr = new ClassID(cls_id);
	state = state->set<SValObjTypeMap>(*ret, cid_ptr);
	C.addTransition(state);
      }
      else if(!funcname.compare("GetObjectClass")) {
	// TODO
	ClassID* cid_ptr = new ClassID("TODO find class name when using GetObjectClass");
	state = state->set<SValObjTypeMap>(*ret, cid_ptr);
	C.addTransition(state);
      }
      else if(!funcname.compare("GetFieldID")) {
	const clang::ento::MemRegion* memreg = Call.getArgSVal(1).getAsRegion();
	if(!memreg) return;

	const clang::ento::ElementRegion* elmtreg = memreg->getAs<clang::ento::ElementRegion>();
	if(!elmtreg) return;

	std::string fld_id = elmtreg->getAsArrayOffset().getRegion()->getString();

	Optional<DefinedSVal> cid_sval = Call.getArgSVal(0).getAs<DefinedSVal>();
	if(!cid_sval) return;
	
	ObjType* const* cid_ptrr = state->get<SValObjTypeMap>(*cid_sval);
	if(!cid_ptrr) return;	
	if((*cid_ptrr)->getType() != CID) return;
	ClassID* cid_ptr = (ClassID*)(*cid_ptrr);

	FieldID* fid_ptr = new FieldID(cid_ptr, fld_id);
	state = state->set<SValObjTypeMap>(*ret, fid_ptr);
	C.addTransition(state);
      }
      else {
	const std::regex getfield_regex("Get(Object|Boolean|Byte|Char|Short|Int|Long|Float|Double)Field");
	if(std::regex_match(funcname, getfield_regex)) {
	  // Get Associated field ID
	  Optional<DefinedSVal> fid_sval = Call.getArgSVal(1).getAs<DefinedSVal>();
	  if(!fid_sval) return;
	  ObjType* const* fid_ptrr = state->get<SValObjTypeMap>(*fid_sval);
	  if(!fid_ptrr) return;
	  if((*fid_ptrr)->getType() != FID) return;
	  FieldID* fid_ptr = (FieldID*)(*fid_ptrr);

	  ProgramStateRef st = state;

	  // Get current counter
	  unsigned counter = st->get<WatchCounter>();
	  // Save the association between the counter and the field
	  st = st->set<WatchMap>(counter, fid_ptr);
	  // Taint the return value
	  st = addTaint(st, *ret, counter);
	  // Update the counter
	  st = st->set<WatchCounter>(counter+1);

	  C.addTransition(st);
	}
      }
    }
  }
}

void NativePointerFieldChecker::checkPreCall(const CallEvent &Call,
					     CheckerContext &C) const {
  ProgramStateRef state = C.getState();
  const std::regex setfield_regex("Set(Object|Boolean|Byte|Char|Short|Int|Long|Float|Double)Field");
  if(Call.getDecl()) {
    if(!strcmp(Call.getDecl()->getDeclKindName(), "CXXMethod")) {
      const FunctionDecl* fd = (const FunctionDecl*) Call.getDecl();
      std::string funcname = fd->getNameInfo().getAsString();
      if(std::regex_match(funcname, setfield_regex)) {
	Optional<DefinedSVal> fid_sval = Call.getArgSVal(1).getAs<DefinedSVal>();
	if(!fid_sval) return;
	ObjType* const* fid_ptrr = state->get<SValObjTypeMap>(*fid_sval);
	if(!fid_ptrr) return;
	if((*fid_ptrr)->getType() != FID) return;
	FieldID* fid_ptr = (FieldID*)(*fid_ptrr);

	if(isSValTainted(Call.getArgSVal(2), state)) {
	  pointerFields.insert(*fid_ptr);
	}
      }
    }
  }
}

std::string jsonStringFormat(const std::string s) {
  std::string ret = s.substr();
  // Remove double quotes
  for(size_t pos=ret.find('"') ; pos!=std::string::npos ; pos=ret.find('"'))
    ret.erase(pos, 1);
  // Surround with double quotes 
  ret = "\"" + ret + "\"";
  return ret;
}

NativePointerFieldChecker::~NativePointerFieldChecker() {
  bool first;
  
  llvm::errs() << "{\n";
  
  llvm::errs() << "\t\"pointerReturnValues\": [";
  first = true;
  for(std::string s : pointerReturnValues) {
    if(first) {
      llvm::errs() << "\n\t\t";
      first = false;
    } else {
      llvm::errs() << ",\n\t\t";
    }
    llvm::errs() << "\"" << s << "\"";
  }
  llvm::errs() << "\n\t],\n";
  
  llvm::errs() << "\t\"pointerArgs\": [";
  first = true;
  for(Arg a : pointerArgs) {
    if(first) {
      llvm::errs() << "\n\t\t";
      first = false;
    } else {
      llvm::errs() << ",\n\t\t";
    }
    llvm::errs() << "{\"meth\":\"" << a.funcname << "\", \"pos\":" << a.position << "}";
  }
  llvm::errs() << "\n\t],\n";

  llvm::errs() << "\t\"pointerFields\": [";
  first = true;
  for(FieldID f : pointerFields) {
    if(first) {
      llvm::errs() << "\n\t\t";
      first = false;
    } else {
      llvm::errs() << ",\n\t\t";
    }
    llvm::errs() << jsonStringFormat(f.cid->class_identifier + ";" + f.field_identifier);
  }
  llvm::errs() << "\n\t]\n}\n";
}


void NativePointerFieldChecker::checkPostStmt(const ImplicitCastExpr *ICE, CheckerContext &C) const {
  const Expr* e = ICE->getSubExpr();
  if(e) {
    const DeclRefExpr* dre = dyn_cast<DeclRefExpr>(e);
    if(dre) {
      const ValueDecl* vd = dre->getDecl();
      if(vd) {
	const ParmVarDecl* pvd = dyn_cast<const ParmVarDecl>(vd);
	if(pvd) {
	  if(pvd->getFunctionScopeDepth() == 0) {

	    Arg* arg_ptr;

	    if(const Decl* d = C.getCurrentAnalysisDeclContext()->getDecl()) {
	      arg_ptr = new Arg(getFunctionNameForDecl(d),
				pvd->getFunctionScopeIndex());
	    }
	    
	    ProgramStateRef st = C.getState();

	    // Get current counter
	    unsigned counter = st->get<WatchCounter>();
	    // Save the association between the counter and the argument
	    st = st->set<WatchMap>(counter, arg_ptr);
	    // Taint the arg value
	    st = addTaint(st, C.getSVal(ICE), counter);
	    // Update the counter
	    st = st->set<WatchCounter>(counter+1);

	    C.addTransition(st);
	  }
	}
      }
    }
  }
}

void NativePointerFieldChecker::checkPreStmt(const CastExpr *CE, CheckerContext &C) const {
  ProgramStateRef st = C.getState();
      
  if(CE->getType().getTypePtr()->isPointerType()) {
    unsigned i, max;
    max = st->get<WatchCounter>();
    for(i=0;i<max;i++) {
      if(isTainted(st, CE->getSubExpr(), C.getLocationContext(), i)) {
	ObjType* const* obj = st->get<WatchMap>(i);

	if(obj) {
	  if((*obj)->getType() == ARG) {
	    Arg* arg = (Arg*)(*obj);
	    pointerArgs.insert(*arg);
	  }
	  else if((*obj)->getType() == FID) {
	    FieldID* fid = (FieldID*)(*obj);
	    pointerFields.insert(*fid);
	  }
	}
	break;
      }
    }
  }
}

void NativePointerFieldChecker::checkPreStmt(const ReturnStmt *RS, CheckerContext &C) const {
  if(RS) {
    const Expr* rv = RS->getRetValue();
    if(rv && containsPointer(rv, C)) {

      const Stmt* cs = C.getStackFrame()->getCallSite();
      if(cs) {
	if(const CallExpr* ce = dyn_cast<CallExpr>(cs)) {
	  if(const Decl* calleeDecl = ce->getCalleeDecl()) {
	    pointerReturnValues.insert(getFunctionNameForDecl(calleeDecl));
	  }
	} else {
	  llvm::errs() << "Error: call site not a CallExpr.\n";
	}
      } else if(C.getStackFrame()->inTopFrame()) {
	if(const Decl* calleeDecl = C.getCurrentAnalysisDeclContext()->getDecl()) {
	  pointerReturnValues.insert(getFunctionNameForDecl(calleeDecl));
	}
      } else {
	llvm::errs() << "Error: no call site and not in top frame.\n";
      }
    }
  }
}

void NativePointerFieldChecker::checkPostStmt(const Expr *E, CheckerContext &C) const {
  ProgramStateRef state = C.getState();

  if(containsPointer(E, C))
    state = state->add<TaintedExpr>(E);
  
  C.addTransition(state);
}

void NativePointerFieldChecker::checkPreStmt(const BinaryOperator *S, CheckerContext &C) const {
  ProgramStateRef state = C.getState();

  if(S->isAssignmentOp())
    if(S->getRHS())
      if(containsPointer(S->getRHS(), C))
  	state = state->add<IsPreTainted>(S);

  C.addTransition(state);
}

void NativePointerFieldChecker::checkPostStmt(const BinaryOperator *S, CheckerContext &C) const {
  ProgramStateRef state = C.getState();

  if(S->isAssignmentOp()) {
    if(S->getLHS()) {
      if(state->contains<IsPreTainted>(S)) {	  
	state = state->remove<IsPreTainted>(S);
	state = state->add<TaintedExpr>(S);  

	Optional<DefinedSVal> lhsSVal = C.getSVal(S->getLHS()).getAs<DefinedSVal>();
	if(lhsSVal)
	  state = state->add<TaintedSVal>(*lhsSVal);

	Optional<Loc> lhsAsLoc = C.getSVal(S->getLHS()).getAs<Loc>();
	if(lhsAsLoc) {
	  StoreManager& sm = state->getStateManager().getStoreManager();
	  SVal storedSVal = sm.getBinding(state->getStore(), *lhsAsLoc);

	  Optional<DefinedSVal> storedDefSVal = storedSVal.getAs<DefinedSVal>();
	  if(storedDefSVal)
	    state = state->add<TaintedSVal>(*storedDefSVal);
	}
      } else {
	Optional<DefinedSVal> lhsSVal = C.getSVal(S->getLHS()).getAs<DefinedSVal>();
	if(lhsSVal)
	  state = state->remove<TaintedSVal>(*lhsSVal);

	Optional<Loc> lhsAsLoc = C.getSVal(S->getLHS()).getAs<Loc>();
	if(lhsAsLoc) {
	  StoreManager& sm = state->getStateManager().getStoreManager();
	  SVal storedSVal = sm.getBinding(state->getStore(), *lhsAsLoc);

	  Optional<DefinedSVal> storedDefSVal = storedSVal.getAs<DefinedSVal>();
	  if(storedDefSVal)
	    state = state->remove<TaintedSVal>(*storedDefSVal);
	}
      }
    }
  }

  C.addTransition(state);
}

void ento::registerNativePointerFieldChecker(CheckerManager &mgr) {
  mgr.registerChecker<NativePointerFieldChecker>();
}

bool ento::shouldRegisterNativePointerFieldChecker(const LangOptions &LO) {
  return true;
}
